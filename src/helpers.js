export function ajax(object, url, callback) {
    let formData = new FormData();
    
    Object.keys(object).map(function(key) {
        return formData.append(key, object[key]);
    });
    
    return fetch(url, {
        method: 'POST',
        body: formData
    })
        .then((response) => response.json())
        .then((responseJson) => {	
            callback(responseJson);
        })
        .catch((error) => {
            console.error(error);
        });
}

export function HelloTester() {

}
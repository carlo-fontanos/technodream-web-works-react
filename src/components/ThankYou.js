import React from 'react'

const THankYou = () => (
	<div className="container">
		<div className="card ml-b text-center">
			<div className="body clearfix">
                <h1>Success</h1>
                <i className="fa fa-5x fa-check-circle m-b" style={{"color": "#81d07a"}}></i>
				<p>Your application and examantion has been submitted.</p>
			</div>
		</div>
	</div>
)

export default THankYou

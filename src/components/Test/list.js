import React, {Component} from 'react'
import Pagination from "react-js-pagination";
import renderHTML from 'react-render-html';
import WOW from "wowjs";
import { withRouter } from "react-router-dom";
import { ajax } from "../../helpers";

class TestList extends Component {
    constructor(props) {
		super(props);
        this.state = {
            loading: false,
            sections: null,
            question: null,
            choices: null,
            applicant_answer: "",
            applicant_choice: null,
            pagination_active: 1,
			pagination_limit: 1,
			pagination_total: 0,
            applicant: localStorage.getItem('applicant')
		}
        
        this.timer = null;
	}
    
    componentWillMount() {
        if(this.state.applicant){
            let current_page = this.props.match.params.number; /* Get current page number from URL */
            if(current_page){
                current_page = parseInt(current_page, 10);
                this.api__get_question(current_page); /* Fetch page */
            } else {
                this.api__get_question(1);
            }
            
            /* Listen to  browser Back and Forward clicks */
            this.unlisten = this.props.history.listen((location, action) => {
                let current_page = this.props.match.params.number; /* Get current page number from URL */
                /* POP is for handling back and forward, while PUSH is for clicks */
                if (action === 'POP'){
                    if(current_page){
                        current_page = parseInt(current_page, 10);
                        this.api__get_question(current_page); /* Fetch page */
                    }
                }
            });
        } else {
            this.props.history.push('/apply');
        }
	}
    
    componentWillUnmount() {
        if(this.state.applicant){
            /* Doing this will cancel the history listener on unmount of component */
            this.unlisten();
        }
    }
    
    componentDidMount() {        
        new WOW.WOW({live: false}).init();
    }
    
    render() {
        let state = this.state;
        
        return (
            <div className="container-fluid">
                <div className="row ml-b">	
            		<div className="col-sm-3">
            			<aside>
            				<div className="list-group">
            					{state.sections && Object.values(state.sections).map((section, i) => {
                                    return <span key={i} className={'list-group-item ' + (section.section_id === state.question.section_id ? 'active': '')}><strong className="m-r">Section {i + 1}:</strong> {section.section_name}</span>
                                })}
            				</div>
                            
                            {state.question && state.question.section_content !== null && 
                                <div className="card">
                                    <div className="body h-a">
                                        {state.question ? renderHTML(state.question.section_content): ''}
                                    </div>
                                </div>
                            }
            			</aside>
            		</div>
            		<div className="col-sm-9">
                        <div className="card">
        					<div className="header clearfix">
        						<h2 className="fl-l">{state.question ? state.question.section_name: ''}</h2>
                                <h2 className="fl-r">{state.pagination_active} / {state.pagination_total}</h2>
        					</div>
        					<div className="body">
                                {state.question && 
                                    <div>
                                        <div className="clearfix">{renderHTML(state.question.question_content)}</div>
                                        
                                        {state.question.question_type === 'multiple' && state.choices &&
                                            <div className="ml-t">
                                                <hr />
                                                <p className="f-b">Choices: </p>
                                                <div>
                                                    {Object.values(state.choices).map((choice, i) => {
                                                        return <div key={i} className="">
                                                            <div className="radio">
                                                                <label>
                                                                    <input 
                                                                        type="radio"
                                                                        name={'choices_' + state.question.question_id}
                                                                        checked={this.state.applicant_choice === parseInt(choice.choice_id, 10)}
                                                                        onChange={this.onChange_choice} 
                                                                        value={choice.choice_id}
                                                                    />
                                                                        {choice.choice_content}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    })}
                                                </div>
                                            </div>
                                        }
                                        
                                        {state.question.question_type === 'input' &&
                                            <div className="ml-t">
                                                <hr />
                                                <p className="f-b">Answer: </p>
                                                <input type="text" className="form-control"name={'input_' + state.question.question_id} onChange={this.onChange_textfield} value={state.applicant_answer} />
                                            </div>
                                        }
                                        
                                        {state.question.question_type === 'essay' &&
                                            <div className="ml-t">
                                                <hr />
                                                <p className="f-b">Answer: </p>
                                                <textarea className="form-control" name={'textarea_' + state.question.question_id} rows="10" onChange={this.onChange_textfield} value={state.applicant_answer} />
                                            </div>
                                        }
                                    </div>
                                }
                            </div>
                            
                            <div className="footer">
                                <div className="clearfix">
                                    <Pagination
                                        activePage={state.pagination_active}
                                        itemsCountPerPage={state.pagination_limit}
                                        totalItemsCount={state.pagination_total}
                                        prevPageText="Prev"
                                        firstPageText="First"
                                        lastPageText="Last"
                                        nextPageText="Next"
                                        hideFirstLastPages={1}
                                        pageRangeDisplayed={1}
                                        itemClassNext={"fl-r  " + ((state.choices && !state.applicant_choice) || (!state.choices && !state.applicant_answer) ? 'd-n': '')}
                                        itemClassPrev="fl-l"
                                        linkClassPrev="btn btn-default btn-lg f-16 f-b"
                                        linkClassNext="btn btn-default btn-lg f-16 f-b"
                                        hideDisabled={1}
                                        activeClass="d-n"
                                        onChange={(page) => {
                                            this.props.history.push(this.props.match.path.replace(':number', page))
                                            this.api__get_question(page);
                                            this.setState({pagination_active: page});
                                        }}
                                    />
                                    
                                    {state.pagination_active === state.pagination_total && ((state.choices && state.applicant_choice) || (!state.choices && state.applicant_answer)) && 
                                        <div className="">
                                            <a className="btn btn-success btn-lg f-16 f-b fl-r" onClick={this.finish}>Finish</a>
                                        </div>
                                    }
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    
    onChange_choice = (e) => {
        var self = this;  
        if(e.target.value && self.state.applicant){
            var applicant = JSON.parse(self.state.applicant);
            var data = Object.assign({
                choice_id: e.target.value,
                question_id: self.state.question.question_id
			}, applicant);
            
            self.setState({applicant_choice: parseInt(e.target.value, 10)});
            
            ajax(data, 'http://192.168.2.15/test/set-answer', function(response){
                if(response.status === 1){
                } else if(response.status === 0){
                    alert(response.msg);
                }
            });
        }
    }
    
    onChange_textfield = (e) => {
        /* Save state but do not trigger ajax update not until user stops typing. */
        clearTimeout(this.timer);
        this.setState({applicant_answer: e.target.value});
        this.timer = setTimeout(this.onChange_textfield_trigger.bind(this), 500);
    }
    
    onChange_textfield_trigger() {
        var self = this;  
        if(self.state.applicant_answer && self.state.applicant){
            var applicant = JSON.parse(self.state.applicant);
            var data = Object.assign({
                answer_content: self.state.applicant_answer,
                question_id: self.state.question.question_id
            }, applicant);
            
            ajax(data, 'http://192.168.2.15/test/set-answer', function(response){
                if(response.status === 1){
                } else if(response.status === 0){
                    alert(response.msg);
                }
            });
        }
    }
    
    finish = (e) => {
        e.preventDefault();
        if(window.confirm('Are you sure you want to finish and submit?')){
            localStorage.removeItem('applicant');
			this.props.history.push('/thankyou');
        }
    }
    
	api__get_question(page_number){
		if(this.state.applicant){
            this.setState({loading: true, applicant_choice: null, applicant_answer: ""});
    		
            var applicant = JSON.parse(this.state.applicant);
            let self = this;
            let page = page_number ? page_number : 1;
            let data = Object.assign({
				page: page,
				limit: self.state.pagination_limit
			}, applicant);
            
            ajax(data, 'http://192.168.2.15/test/get-question', function(response){
                /* Update state of this component */
                self.setState({
                    loading: false,
                    sections: response.sections,
                    question: response.question,
                    choices: response.choices,
                    applicant_answer: response.applicant_answer ? response.applicant_answer.answer_content: "",
                    applicant_choice: response.applicant_answer ? parseInt(response.applicant_answer.choice_id, 10): null,
                    pagination_total: response.total_items,
                    pagination_active: page, /* Update pagination nav */
                }, function() {
                    window.scrollTo(0, 0);
                    new WOW.WOW({live: false}).init(); /* Re-initialize wowjs */
                });
                
                
            });
        }
	}
}

export default withRouter(TestList);

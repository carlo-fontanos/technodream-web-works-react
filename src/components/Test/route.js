import React from 'react'
import { Switch, Route } from 'react-router-dom'
import TestList from './list'
import TestIndex from './index'

// The Items component matches one of two different routes
// depending on the full pathname
const RouteHandler = () => (
    <Switch>
        <Route exact path='/test' component={TestIndex}/>
        <Route exact path='/test/page/:number' component={TestList}/>
    </Switch>
)


export default RouteHandler

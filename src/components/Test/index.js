import React, {Component} from 'react'
import { withRouter } from "react-router-dom";
import renderHTML from 'react-render-html';
import { Link } from 'react-router-dom';
import { ajax } from "../../helpers";

class TestIndex extends Component {
    constructor(props) {
		super(props);
        this.state = {
            loading: false,
            test: null,
            applicant: localStorage.getItem('applicant')
		}
	}
    
    componentWillMount() {
        var self = this;
        if(self.state.applicant){
            var applicant = JSON.parse(self.state.applicant);
            ajax({test_id: applicant.test_id}, 'http://192.168.2.15/test/get-test', function(response){
                if(response.status === 1){
                    self.setState({test: response.msg});
                } else if(response.status === 0){
                    self.setState({error: response.msg});
                }
                window.scrollTo(0, 0);
            });
        } else {
            self.props.history.push('/apply');
        }
    }
    
    render() {
        let state = this.state;
        
        return (
            <div className="container">
        		<div className="card ml-b">
        			<div className="body clearfix">
						{state.test && state.test.test_content !== null && 
							<div>
								{state.test ? renderHTML(state.test.test_content): ''}
							</div>
						}
        			</div>
        			<div className="footer">
                        <Link to="/test/page/1" className="btn bg-blue btn-lg pl-lr">Start Exam</Link>
        			</div>
        		</div>
        	</div>
        );
    }
    

}

export default withRouter(TestIndex);

import React from 'react'

const THankYou = () => (
	<div className="container">
		<div className="card ml-b text-center">
			<div className="body clearfix">
                <h1>404 Not Found</h1>
                <i className="fa fa-5x fa-times-circle m-b" style={{"color": "#f56b6b"}}></i>
				<p>The page you were looking for was not found.</p>
			</div>
		</div>
	</div>
)

export default THankYou

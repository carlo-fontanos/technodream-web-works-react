import React from 'react'
import { Link } from 'react-router-dom';

const Home = () => (
	<div className="container">
		<div className="card ml-b">
			<div className="header">
				<h2>Werlcome</h2>
			</div>
			<div className="body clearfix">
				<div className="col-md-4 text-center">
					<img src={"./img/logo.png"} className="ml-t" alt="" />
				</div>
				<div className="col-md-8 ml-b">
					<h1 className="ml-b">Technodream Web Works</h1>
					<p>We are a leading Web Development Enterprise that focuses on developing and hosting websites. Our methods and tools are innovative and up-to- date to surpass industry standards; not to mention that we also houses nothing but the best sales and customer service representatives.</p>
					<p>We are equally competitive in providing our employees the highest compensation, as well as the necessary training to advance their career further.</p>
					<Link to="/apply" className="btn btn-lg bg-blue ml-tb">Apply Now</Link>
				</div>
			</div>
		</div>
	</div>
)

export default Home

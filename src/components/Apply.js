import React, { Component } from 'react';
import { ajax } from '../helpers';

export default class Apply extends Component {
    constructor(props) {
		super(props);
        this.state = {
            loading: false,
            error: ""
		}
	}

    componentWillMount(){
        /* Do not allow to visit this page if already alrady registered applicant. */
        if(localStorage.getItem('applicant')){
            this.props.history.push('/test');
        }
    }

    onChange = (e) => {
        var state = this.state;
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    onChange_department = (e) => {
        var self = this;
        self.setState({department_id: e.target.value, test_id: 0});

        if(e.target.value > 0){
            ajax({department_id: e.target.value}, 'http://192.168.2.15/test/get-tests', function(response){
                if(response.status === 1){
                    self.setState({tests: response.tests});
                } else if(response.status === 0){
                    self.setState({error: response.msg});
                }
            });
        }
    }

    onSubmit = (e) => {
        e.preventDefault();
        var self = this;
        ajax(this.state, 'http://192.168.2.15/applicants/store', function(response){
            if(response.status === 1){
                /* Save applicant's data to local storage for reference later on the exam */
                localStorage.setItem('applicant', JSON.stringify({
					user_id: response.user_id,
					email: self.state.email,
					phone: self.state.phone,
					first_name: self.state.first_name,
					middle_name: self.state.middle_name,
					last_name: self.state.last_name,
                    department_id: self.state.department_id,
					test_id: self.state.test_id,
                    working_time: self.state.working_time
				}));

                self.props.history.push('/test');

            } else if(response.status === 0){
                self.setState({error: response.msg});
                document.getElementById("errors").scrollIntoView(false);
            }
        });
    }

    render() {
        return (
        	<div className="container">
        		<div className="card ml-b">
        			<div className="header">
        				<h2>Apply</h2>
        				<small>Technodream Web Works</small>
        			</div>
        			<div className="body clearfix">
                        <div className="col-md-4 p-0">
                            {this.state.error &&
                                <div className="alert alert-danger" id="errors">
                                    <strong>Error:</strong> {this.state.error}
                                </div>
                            }

                            <div className="form-group">
                                <label htmlFor="working_time">Working Time <i className="fa fa-asterisk text-danger"></i></label>
                                <select className="form-control" name="working_time" onChange={this.onChange}>
                                    <option value="">Select Working Time</option>
                                    <option value="night-time">Night Time</option>
                                    <option value="day-time">Day Time</option>
                                </select>
                            </div>

                            <div className="form-group">
                				<label htmlFor="department_id">Position <i className="fa fa-asterisk text-danger"></i></label>
                				<select className="form-control" name="department_id" onChange={this.onChange_department}>
                                    <option value="0">Select Position</option>
                                    <option value="4">Call Center</option>
                                    <option value="3">Human Resource</option>
                                    <option value="5">Sales</option>
                                    <option value="6">Graphic Design</option>
                                    <option value="1">Programming</option>
                                    <option value="2">SEO</option>
                                </select>
                            </div>

                            <div className="form-group">
                				<label htmlFor="test_id">Test <i className="fa fa-asterisk text-danger"></i></label>
                				<select className="form-control" name="test_id" onChange={this.onChange} value={this.state.test_id}>
                                    <option value="0">Select Test</option>
                                    {this.state.tests && Object.values(this.state.tests).map((test, i) => {
                                        return <option key={i} value={test.test_id}>{test.test_name}</option>
                                    })}
                                </select>
                            </div>
                            <div className="form-group">
                				<label htmlFor="first_name">First Name <i className="fa fa-asterisk text-danger"></i></label>
                				<input type="text" className="form-control" name="first_name" id="first_name" onChange={this.onChange} />
                            </div>
							 <div className="form-group">
                				<label htmlFor="middle_name">Middle Name <i className="fa fa-asterisk text-danger"></i></label>
                				<input type="text" className="form-control" name="middle_name" id="middle_name" onChange={this.onChange} />
                            </div>
                            <div className="form-group">
                				<label htmlFor="last_name">Last Name <i className="fa fa-asterisk text-danger"></i></label>
                				<input type="text" className="form-control" name="last_name" id="last_name" onChange={this.onChange} />
                            </div>
                            <div className="form-group">
                				<label htmlFor="email">Email <i className="fa fa-asterisk text-danger"></i></label>
                				<input type="email" className="form-control" name="email" id="email" onChange={this.onChange} />
                            </div>
                            <div className="form-group">
                				<label htmlFor="phone">Phone Number <i className="fa fa-asterisk text-danger"></i></label>
                				<input type="text" className="form-control" name="phone" id="phone" onChange={this.onChange} />
                            </div>
                        </div>
        			</div>
        			<div className="footer">
        				<button className="btn btn-default pl-lr" onClick={this.onSubmit}>Begin Exam</button>
        			</div>
        		</div>
        	</div>
        )
    }
}
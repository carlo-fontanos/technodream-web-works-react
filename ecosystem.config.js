module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [
    {
      name      : 'technodream-web-works-react',
      script    : 'npm',
      args      : 'run start'
    }
  ]
};
